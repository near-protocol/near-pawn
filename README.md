# NEAR PAWN #



### What is NEAR PAWN? ###

**NEAR PAWN** (**P**ostman **A**PI **W**orkspace **N**exus) is a public workspace consisting of **NEAR** APIs, SDKs, documentation, dev tools, and dApps.